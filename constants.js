module.exports = {
  MIN_CASES: 250,
  MAX_CASES: 1000,
  MIN_EVENTS: 2000,
  MAX_EVENTS: 10000,
};
