const { v4: uuidv4 } = require("uuid");
const { sampleOne, random } = require("utilist");
const { EVENT_TYPES } = require("./eventTypes");
const subDays = require("date-fns/subDays");
const getUnixTime = require("date-fns/getUnixTime");

const generateCases = (amount = 100) => {
  const cases = [];
  for (let index = 0; index < amount; index++) {
    cases[index] = uuidv4();
  }
  return cases;
};

const generateEvents = (cases, amount = 500, sinceInMillis) => {
  const events = [];
  for (let index = 0; index < amount; index++) {
    events[index] = {
      caseUuid: sampleOne(cases),
      event: sampleOne(EVENT_TYPES),
      timestamp: !!sinceInMillis
        ? generateRandomDateWithStartAndEnd(sinceInMillis)
        : generateRandomDate(),
    };
  }
  return events;
};

const generateRandomDate = (endDate = Date.now(), daysInInterval = 7) => {
  const startDate = subDays(endDate, daysInInterval);
  return random(getUnixTime(startDate), getUnixTime(endDate), true);
};

const generateRandomDateWithStartAndEnd = (
  startDateInMillis,
  endDateInMillis = Date.now()
) => {
  return random(
    getUnixTime(startDateInMillis),
    getUnixTime(endDateInMillis),
    true
  );
};

module.exports = { generateCases, generateEvents };
