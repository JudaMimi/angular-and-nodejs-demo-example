const express = require('express');
var cors = require('cors')

const bodyParser = require("body-parser");

const app = express();
app.use(cors());
// parse requests of content-type - application/json
app.use(bodyParser.json());

require("make-promises-safe"); // installs an 'unhandledRejection' handler

const fastify = require("fastify")({
  logger: true,
});
const { random } = require("utilist");

const { generateCases, generateEvents } = require("./utils");

const { MIN_CASES, MAX_CASES, MIN_EVENTS, MAX_EVENTS } = require("./constants");

fastify.get("/status", async (_request, _reply) => {
  return { success: true };
});
app.get("/events", async (request, _reply) => {
  const numberOfCases = random(MIN_CASES, MAX_CASES, true);
  fastify.log.info(`Generating ${numberOfCases} cases.`);
  const cases = generateCases(numberOfCases);
  let numberOfEvents = 0;

  if (request.query && request.query.since) {
    const sinceInMillis = parseInt(request.query.since) * 1000;
    const fromSinceToNowInMillis = Date.now() - sinceInMillis;
    numberOfEvents = random(0, fromSinceToNowInMillis, true);
    if (numberOfEvents > 100) {
      numberOfEvents = 100;
    }
    fastify.log.info(
      `${fromSinceToNowInMillis} the value of since to now in millis.`
    );
    fastify.log.info(`Generating ${numberOfEvents} events.`);
    return _reply.send( generateEvents(cases, numberOfEvents, sinceInMillis));
   // return generateEvents(cases, numberOfEvents, sinceInMillis);
  }
  numberOfEvents = random(MIN_EVENTS, MAX_EVENTS, true);
  fastify.log.info(`Generating ${numberOfEvents} events.`);
   return _reply.send( generateEvents(cases, numberOfEvents));
})

app.use(express.static('./dist/nect'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'dist/nect/'}),
);

// set port, listen for requests
const start = async () => {
  const PORT = process.env.PORT || 8080;
  try {
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}.`);
    });
  } catch (err) {
    app.log.error('error in launching ',err);
    process.exit(1);
  }
};
start();