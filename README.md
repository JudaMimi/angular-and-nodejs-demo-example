# Angular and NodeJS Demo Example using a random API
This project is a web application Demo using some random API to generate data and integrate it\
with the front-end app built with Angular and NodeJS as a backend.\
this application needs angular CLI and NodeJS installed in your machine to run on localhost.


## Get Started

in the root of the project please run those commands.
- `npm install`
- `ng build --prod` The build artifacts will be stored in the `dist/` directory.
- `node server` The app will be running on port 8080 (`http://localhost:8080`)

## App description

Frontend: built with angular\
 --> The app use the api '/events' to retreive the events\
 --> Sort the events based on event type\
 --> Sort the events based on short UUID\
 --> Refresh the app periodically (10000ms) (Since it was not written how much time I choose 10000ms)\
 --> The app uses Angular Material theming.
Backend: Nodejs \
 --> uses the webservice (/events) and connects with the fronend calls.\
 --> responsible of launching the app. \


## Production

I deployed the app on heroku.
The application runs on [Heroku](https://juda-nect-test.herokuapp.com/home)

## Running unit tests

Run `ng test` to execute the unit tests.

## Author
Juda Buchahda Angular and NodeJS API example
Novembre 2020
