import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { isThisISOWeek } from 'date-fns';
import { EventService  } from '../../services/event.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  events: any[];
  type: any[];
  uiidArr: any[];
  typeList: any = [];
  uuidList: any = [];
  dataSource: any[];
  all: any[];
  fromType = false;
  fromUUid = false;
  tableLength = 400;
  indexCounter = 0;
  interval: any;
  displayedColumns: string[] = ['position', 'event', 'caseUuid', 'timestamp'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private eventService: EventService) { }

  ngOnInit(): void {
    this.getEvents();
  }

  getEvents(): void {
    this.fromUUid = false;
    this.fromType = false;
    this.eventService.getEvents()
    .subscribe((data: any) => {
        this.events = data;
        // console.log('ff ', data.sort((a, b) => a.timestamp - b.timestamp));
        this.events = data.sort((a, b) => a.timestamp - b.timestamp);
        this.all = data;
        this.tableLength = data.length;
        this.getEventType(this.events);
        this.getUuidList(this.events);
        this.getPerPage(1);
    });
  }
  sortByType(type: string): void {
    // tslint:disable-next-line:variable-name
    // tslint:disable-next-line:only-arrow-functions
    this.type = this.events.filter(function(event) {
      return event.event ===  type;
    });
    this.tableLength = this.type.length;
    this.dataSource = this.type;
    this.fromUUid = false;
    this.fromType = true;
    this.paginator.firstPage();
    this.getPerPage(1);
  }
  sortByUUID(uuid: string): void {
    // tslint:disable-next-line:variable-name
    // tslint:disable-next-line:only-arrow-functions
    this.uiidArr = this.events.filter(function(event) {
      return event.caseUuid.split('-')[0] ===  uuid;
    });
    this.tableLength = this.uiidArr.length;
    this.dataSource = this.uiidArr;
    this.fromUUid = true;
    this.fromType = false;
    this.paginator.firstPage();
    this.getPerPage(1);
  }

  getPerPage(page: number) {
    const start = (page - 1) * 10  ;
    const end = 10 * page;
    if (page >= 2) {
      this.indexCounter = 10 * (page - 1);
    } else {
      this.indexCounter = 0;
    }
    this.dataSource = this.all.slice(start, end);
    if (this.fromType) {
      this.dataSource = this.type.slice(start, end);
    }
    if (this.fromUUid) {
      this.dataSource = this.uiidArr.slice(start, end);
    }
  }
  onPaginateChange(pageIndex): void {
    const page = pageIndex.pageIndex + 1;
    this.getPerPage(page);
  }
  getEventType(data: any) {
    let i = 0;
    const result = data.reduce((unique, o) => {
      if (!unique.some(obj => obj.event === o.event)) {
        unique.push(o);
      }
      return unique;
    }, [] );
    for (i; i < result.length ; i++) {
      this.typeList.push(result[i].event);
    }
  }
  getUuidList(data: any) {
    let i = 0;
    const result = data.reduce((unique, o) => {
      if (!unique.some(obj => obj.caseUuid === o.caseUuid)) {
        unique.push(o);
      }
      return unique;
    }, [] );
    for (i; i < result.length ; i++) {
      this.uuidList.push(result[i].caseUuid.split('-')[0]);
    }
  }
  refresh(event): void {
    if (event.checked) {
      this.interval = setInterval(() => {
        this.getEvents();
      }, 10000);
    }
    if (!event.checked) {
      console.log('stop');
      clearInterval(this.interval);
    }
  }
}
