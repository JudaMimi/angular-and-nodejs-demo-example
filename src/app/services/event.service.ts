import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventService {
    // private eventUrl = 'https://juda-nect-test.herokuapp.com/events'; // production
   private eventUrl = 'http://localhost:8080/events';
   httpOptions = {
     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
   };

  constructor(private http: HttpClient) { }

  /** GET EVENTS */
  getEvents(): Observable<any> {
    const url = `${this.eventUrl}`;
    return this.http.get<any>(url).pipe(
      tap(res => this.log( res )),
      catchError(this.handleError<any>(`get events`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(operation, 'error:  ', error.error.message); // log to console instead
      return of(result as T);
    };
  }

  /** Log a message */
  private log(res) {
   console.log(`event service: `, res );
  }
}
